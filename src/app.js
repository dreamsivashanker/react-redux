"use strict";
import React from 'react';
import {render} from 'react-dom';
import {createStore, applyMiddleware} from 'redux';
import { createLogger } from 'redux-logger'
import {Provider} from 'react-redux';
//STEP 3 Reducers
import reducers from "./reducers/index";

//actions
import {addToCart} from './actions/cartActions';
import {updateBooks, deleteBooks, postBooks} from './actions/booksAction';

const middleware = applyMiddleware(createLogger());

//STEP 1 create a store
const store = createStore(reducers,middleware);

import BooksList from './components/pages/booksList';

render( 
        <Provider store={store}>
        <BooksList/>
        </Provider>
        ,document.getElementById("root")
        )
//STEP 2 create and dispatch the actions
//store.dispatch(getBooks(
//            [{
//            id: 1,
//            title: "this is book first title",
//            description: "this is the first book description",
//            price: 44
//            },
//            {
//            id: 2,
//            title: "this is book second title",
//            description: "this is the second book description",
//            price: 50
//            }]
//            ))
//store.dispatch(postBooks(
//            [{
//            id: 1,
//            title: "this is book first title",
//            description: "this is the first book description",
//            price: 44
//            },
//            {
//            id: 2,
//            title: "this is book second title",
//            description: "this is the second book description",
//            price: 50
//            }]
//            ))
//store.dispatch(deleteBooks({id: 1}))
//
//store.dispatch(updateBooks({id: 2,title : "2nd title"}))
//
////add to cart
//store.dispatch(addToCart([{id : 2}]))
