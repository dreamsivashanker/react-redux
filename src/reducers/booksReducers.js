"use strict";

// books reducers

//STEP 3 define the reducers
export function booksReducers(state = 
            { 
            books : [{
            id: 1,
            title: "this is book first title",
            description: "this is the first book description",
            price: 44.33
            },
            {
            id: 2,
            title: "this is book second title",
            description: "this is the second book description",
            price: 55
            }]
            },action){
    switch(action.type)
    {
        case "GET_BOOKS" : 
            return {...state,books:[...state.books]};break;
        case "POST_BOOK" : 
            return {books : [...state.books,...action.payload]};break;
        case "DELETE_BOOK" : 
            //create copy of current array of books
            const booktodelete = [...state.books];
            //determine at which index in books array is the bookto be deleted
            const indextodelete = booktodelete.findIndex(
                function (book) {
                    return book.id === action.payload.id;
                }
            )
            //use slice to remove specific index
            return {
                books: [...booktodelete.slice(0, indextodelete),
                ...booktodelete.slice(indextodelete + 1)]
            }
            break;
        case "UPDATE_BOOK":
            //create a copy
            const booktoupdate = [...state.books];
            const indextoUpdate = booktoupdate.findIndex(
                function (book) {
                    return book.id === action.payload.id;
                }
            )

            const newbooktoupdate = {
                ...booktoupdate[indextoUpdate],
                title: action.payload.title
            }

            return {
                books: [...booktoupdate.slice(0, indextoUpdate), newbooktoupdate,
                    ...booktoupdate.slice(indextoUpdate + 1)
                ]
            }
            break;
    }
    return state;
}

//export default booksReducers;


